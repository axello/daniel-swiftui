//
//  GreetingLabel.swift
//  HelloSwiftUI2
//
//  Created by Axel Roest on 07/10/2019.
//  Copyright © 2019 Axel Roest. All rights reserved.
//

import SwiftUI

struct GreetingLabel: View {
    let greeting: String
    
    var body: some View {
        Text(greeting)
            .font(.title)
            .foregroundColor(.secondary)
    }
    
    init(_ greeting: String) {
        self.greeting = greeting
    }
}

struct GreetingLabel_Previews: PreviewProvider {
    static var previews: some View {
        GreetingLabel("Howdy")
    }
}
