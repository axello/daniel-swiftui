//
//  GreetingButton.swift
//  HelloSwiftUI2
//
//  Created by Axel Roest on 07/10/2019.
//  Copyright © 2019 Axel Roest. All rights reserved.
//

import SwiftUI

struct GreetingButton: View {
    @State private var buttonIsDisabled = false
    @Binding var greeting: String
    
    var body: some View {
        Button("Press Here") {
            self.greet()
        }.disabled(buttonIsDisabled)

    }

    private func greet() {
        buttonIsDisabled = true
        greeting = "Hello World!"
    }
}

struct GreetingButton_Previews: PreviewProvider {
    static var previews: some View {
        GreetingButton(greeting: .constant("Hey"))
    }
}
