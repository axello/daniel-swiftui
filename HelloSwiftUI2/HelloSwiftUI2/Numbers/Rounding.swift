//
//  Rounding.swift
//  HelloSwiftUI2
//
//  Created by Axel Roest on 07/10/2019.
//  Copyright © 2019 Axel Roest. All rights reserved.
//

import Foundation

@propertyWrapper

struct RoundedTo {
    private var value: Double = 0
    private var multiplier: Double = 1
    
    var wrappedValue: Double {
        get {
            value
        }
        set {
            value = (newValue * multiplier).rounded() / multiplier
        }
    }
    
    init(wrappedValue initialValue: Double) {
        self.wrappedValue = initialValue
    }

    init(wrappedValue initialValue: Double, _ precision: Int) {
        guard precision >= 0 && precision <= 3 else {
            fatalError("rounding is only supported for precision 0, 1, 2, 3")
        }
        multiplier = [1, 10, 100, 1000][precision]
//        let dec = pow( Decimal(10), precision)
//        let nsd = NSDecimalNumber(decimal: dec)
//        let mul = nsd.intValue
        self.wrappedValue = initialValue
    }

}
