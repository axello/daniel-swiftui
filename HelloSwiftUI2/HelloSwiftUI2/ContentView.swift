//
//  ContentView.swift
//  HelloSwiftUI2
//
//  Created by Axel Roest on 07/10/2019.
//  Copyright © 2019 Axel Roest. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    @State private var greeting = "Hello"
    @State private var value = 2.0
    
    var body: some View {
        VStack {
            GreetingLabel(greeting)
            GreetingButton(greeting: $greeting)
                .padding()

//            Button("Press Here as well") {        // simple button; cannot be stylized
//                print("Tapped as well")
//            }.padding()
            
            ValueView(value: value)
            ValueSlider(value: $value)
            ValueSlider(value: $value)
                .accentColor(Color.green)
//                .background(Color.red)
//                .Color(grayscale(0.2))
        }
    }
    
    private func printTapped() {
        print("Tapped")
    }
    
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
