//
//  LengthWarningView.swift
//  MyMood
//
//  Created by Axel Roest on 09/10/2019.
//  Copyright © 2019 Axel Roest. All rights reserved.
//

import SwiftUI

struct LengthWarningView: View {
    
    var body: some View {
        Text("(Must be less than 30 characters)")
            .foregroundColor(.red)
            .padding(.bottom)
    }
}

struct LengthWarningView_Previews: PreviewProvider {
    static var previews: some View {
        LengthWarningView()
    }
}
