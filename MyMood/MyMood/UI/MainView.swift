//
//  ContentView.swift
//  MyMood
//
//  Created by Axel Roest on 08/10/2019.
//  Copyright © 2019 Axel Roest. All rights reserved.
//

import SwiftUI

struct MainView: View {
    var history = History()
    
    var body: some View {
        TabView {
            MoodInputView(presenter: MoodInputPresenter(history))
                .tabItem {
                    VStack {
                        Text("Mood")
                        Image(systemName: "person.circle")
                    }
                }
            HistoryView(history: history)
                .tabItem {
                    VStack {
                        Text("History")
                        Image(systemName: "tray.2")
                    }
                }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        MainView()
    }
}
