//
//  MoodCell.swift
//  MyMood
//
//  Created by Axel Roest on 09/10/2019.
//  Copyright © 2019 Axel Roest. All rights reserved.
//

import SwiftUI

struct MoodCell: View {
    let presenter: MoodPresenter

    var body: some View {
        HStack {
            Text(presenter.context).padding(.leading)
            Spacer()
            Text(presenter.rating).padding(.trailing)
        }
    }
}

struct MoodCell_Previews: PreviewProvider {
    static var previews: some View {
        MoodCell(presenter: MoodPresenter(mood: nil))
    }
}
