//
//  MoodDetailView.swift
//  MyMood
//
//  Created by Axel Roest on 10/10/2019.
//  Copyright © 2019 Axel Roest. All rights reserved.
//

import SwiftUI

struct MoodDetailView: View {
    let presenter: MoodPresenter
    
    var body: some View {
        VStack {
            Text(presenter.context)
                .font(.largeTitle)
                .foregroundColor(.secondary)
                .padding(.top)
            FaceView(value: presenter.rawRating)
            Text(presenter.emotion)
                .font(.headline)
                .padding(.bottom)
            .padding()
            Text(presenter.date)
        }.navigationBarTitle("Rating: \(presenter.rating)")
    }
}

struct MoodDetailView_Previews: PreviewProvider {
    static var previews: some View {
        MoodDetailView(presenter: MoodPresenter(mood: nil))
    }
}
