//
//  HistoryView.swift
//  MyMood
//
//  Created by Axel Roest on 08/10/2019.
//  Copyright © 2019 Axel Roest. All rights reserved.
//

import SwiftUI

struct HistoryView: View {
    @ObservedObject var history: History
    
    var presenters: [MoodPresenter] {
//        history.moods.map{ mood in MoodPresenter(mood: mood) }
//        history.moods.map{ MoodPresenter(mood: $0) }    // similar
        history.moods.map( MoodPresenter.init(mood:) )      // similar
    }
    
    var body: some View {
        NavigationView {
            List {
                ForEach(presenters) { presenter in
                    NavigationLink(destination: MoodDetailView(presenter: presenter)) {
                        MoodCell(presenter: presenter)
                    }
                }.onDelete { index in
                    self.history.remove(at: index)
                }
            }.navigationBarTitle("Mood History")
                .navigationBarItems(leading: Button("Clear", action: self.clearHistory),
                                    trailing: EditButton() )
        }
    }
}

// cleaner: extract action out of body
extension HistoryView {
    private func clearHistory() {
        history.clear()
    }
}

struct HistoryView_Previews: PreviewProvider {
    static var previews: some View {
        HistoryView(history: History())
    }
}
