//
//  MoodInputButton.swift
//  MyMood
//
//  Created by Axel Roest on 08/10/2019.
//  Copyright © 2019 Axel Roest. All rights reserved.
//

import SwiftUI

struct MoodInputButton: View {
    let contextIsNotValid: Bool
    let save: () -> Void
        
    var body: some View {
        Button("Record Mood", action: save)
            .disabled(contextIsNotValid)
    }
}

struct MoodInputButton_Previews: PreviewProvider {
    static var previews: some View {
        MoodInputButton(contextIsNotValid: true, save: { } )
    }
}
