//
//  ValueSlider.swift
//  MyMood
//
//  Created by Axel Roest on 08/10/2019.
//  Copyright © 2019 Axel Roest. All rights reserved.
//

import SwiftUI

struct ValueSlider: View {
    @Binding var value: Double
    @Binding var rating: Double
    
    var body: some View {
        Slider(value: $value, in: 0.0 ... 4.0, step: 0.1) { isSliding in
            if !isSliding {
                self.rating = self.value
            }
        }.padding()
    }
}

struct ValueSlider_Previews: PreviewProvider {
    static var previews: some View {
        ValueSlider(value: .constant(3), rating: .constant(3))
    }
}
