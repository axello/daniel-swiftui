//
//  MoodInputView.swift
//  MyMood
//
//  Created by Axel Roest on 08/10/2019.
//  Copyright © 2019 Axel Roest. All rights reserved.
//

import SwiftUI

struct MoodInputView: View {
    @State private var value = 2.0
    @ObservedObject var presenter: MoodInputPresenter
    
    var body: some View {
        VStack {
            Group {
                ContextInputField(context: $presenter.context)
                presenter.showWarningViewIfNecessary
            }.animation(.easeIn)
            
            FaceView(value: value)
                // .scaleEffect(x: 1, y: 2/(0.1 + self.value) , anchor: .center)
            ValueSlider(value: $value, rating: $presenter.rating)
            MoodInputButton(contextIsNotValid: presenter.contextIsNotValid, save: (presenter.saveMood))
        }
    }
}

struct MoodInputView_Previews: PreviewProvider {
    static var previews: some View {
        MoodInputView(presenter: MoodInputPresenter(History()))
    }
}
