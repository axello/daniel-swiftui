//
//  FaceView.swift
//  MyMood
//
//  Created by Axel Roest on 14/10/2019.
//  Copyright © 2019 Axel Roest. All rights reserved.
//

import SwiftUI

struct FaceView: View {
    let value: Double
    
    var body: some View {
        GeometryReader { proxy in
            
        ZStack {
            Head(value: self.value)
            Eyes(side: self.minDimension(for: proxy))
            Mouth(value: self.value, side: self.minDimension(for: proxy))
        }   // .scaleEffect(x: 1, y: 2/(0.1+self.value) , anchor: .center)
        }
    }
    
    func minDimension(for proxy: GeometryProxy) -> CGFloat {
        let size = proxy.frame(in: .local).size
        return min(size.width, size.height)
    }
}

struct FaceView_Previews: PreviewProvider {
    static var previews: some View {
        FaceView(value: 2.7)
    }
}
