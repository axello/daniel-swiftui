//
//  Mouth.swift
//  MyMood
//
//  Created by Axel Roest on 14/10/2019.
//  Copyright © 2019 Axel Roest. All rights reserved.
//

import SwiftUI

struct Mouth: View {
    var width: CGFloat { side * 3 / 4 }
    var height: CGFloat { side / 4 }
    var lineWidth: CGFloat { side / 40 }
    var startingPoint: CGPoint { CGPoint(x: 0, y: height / 2) }
    var endingPoint: CGPoint { CGPoint(x: width, y: height / 2) }
//    var centerPoint: CGPoint { CGPoint(x: width / 2, y: CGFloat(value) * height / 4) }
    
    var yForControlPoints: CGFloat {
        CGFloat(value) * height / 4
    }
    var leftControlPoint:  CGPoint {CGPoint(x: width / 4, y: yForControlPoints)}
    var rightControlPoint: CGPoint {CGPoint(x: width * 3 / 4, y: yForControlPoints)}

    let value: Double
    let side: CGFloat
    
    var body: some View {
        path
            .stroke(lineWidth: lineWidth)
            .frame(width: width, height: height, alignment: .center)
            .offset(x: 0, y: height / 2)
            .foregroundColor(.black)
    }
    
    var path: Path {
        var path = Path()
        path.move(to: startingPoint)
        path.addCurve(to: endingPoint, control1: leftControlPoint, control2: rightControlPoint)
//        path.addLine(to: centerPoint)
//        path.addLine(to: endingPoint)
        return path
    }
}

struct Mouth_Previews: PreviewProvider {
    static var previews: some View {
        Mouth(value: 2.7, side: 300)
    }
}
