//
//  Eyes.swift
//  MyMood
//
//  Created by Axel Roest on 14/10/2019.
//  Copyright © 2019 Axel Roest. All rights reserved.
//

import SwiftUI

struct Eyes: View {
    let side: CGFloat
    
    var body: some View {
        HStack {
            Circle().foregroundColor(.black)
            Spacer(minLength: 100)
            Circle().foregroundColor(.black)
        }
        .frame(width: side * 3/4, height: side / 12, alignment: .center)
        .offset(x: 0, y: -side / 8)
    }
}

struct Eyes_Previews: PreviewProvider {
    static var previews: some View {
        // Eyes()
        FaceView(value: 2.7)
    }
}
