//
//  MoodPresenter.swift
//  MyMood
//
//  Created by Axel Roest on 09/10/2019.
//  Copyright © 2019 Axel Roest. All rights reserved.
//

import Foundation

struct MoodPresenter {
    let mood: Mood
}

fileprivate let dateFormatter: DateFormatter = {
    let formatter = DateFormatter()
    formatter.dateStyle = .medium
    formatter.timeStyle = .short
    return formatter
}()

extension MoodPresenter {
    var rating: String {
        mood.rating.toOnePlace
    }
    
    var context: String {
        mood.context
    }
    
    var emotion: String {
        mood.emotion.rawValue
    }
    
    var date: String {
        dateFormatter.string(from: mood.timeStamp)
    }
    
    var rawRating: Double {
        mood.rating
    }
}

extension MoodPresenter {
    init(mood: Mood?) {
        if let mood = mood {
            self.mood = mood
        } else {
            self.mood = Mood(rating: -1.0, context: "No mood today 😐")
        }
    }
}

extension MoodPresenter: Identifiable {
    var id: UUID {
        mood.id
    }
}
