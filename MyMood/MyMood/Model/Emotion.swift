//
//  Emotion.swift
//  MyMood
//
//  Created by Axel Roest on 09/10/2019.
//  Copyright © 2019 Axel Roest. All rights reserved.
//

import Foundation

enum Emotion: String, CaseIterable {
    case despondent
    case sad
    case neutral
    case happy
    case joyous
}

extension Emotion {
    static func create(with rating: Double) -> Emotion {
        guard (rating >= 0 && rating <= 4) else {
            return .neutral
        }
        return Emotion.allCases[Int(rating.rounded())]
    }
}
