//
//  Mood.swift
//  MyMood
//
//  Created by Axel Roest on 08/10/2019.
//  Copyright © 2019 Axel Roest. All rights reserved.
//

import SwiftUI

struct Mood: Codable, Identifiable {
    let rating: Double
    let context: String
    let timeStamp = Date()
    let id = UUID()
    
    var emotion: Emotion {
        Emotion.create(with: rating)
    }
}
