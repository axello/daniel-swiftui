//
//  ToOnePlace.swift
//  MyMood
//
//  Created by Axel Roest on 08/10/2019.
//  Copyright © 2019 Axel Roest. All rights reserved.
//

import Foundation

extension Double {
    var toOnePlace: String {
        formatter.string(for: self) ?? "Error formatting Double"
    }
}

fileprivate let formatter: NumberFormatter = {
    let f = NumberFormatter()
    f.maximumFractionDigits = 1
    f.minimumFractionDigits = 1
    f.minimumIntegerDigits = 1
    return f
}()
