//
//  ContentView.swift
//  HelloSwiftUI2
//
//  Created by Axel Roest on 07/10/2019.
//  Copyright © 2019 Axel Roest. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    @EnvironmentObject private var customTint: CustomTint

    var body: some View {
        TabView {
//            TintSelector()
            ValueInputView()
                .tabItem {
                    VStack {
                        Text("Input")
                        Image(systemName: "arrow.right.arrow.left")
                    }
                }
            LastRecordedValue()
                .tabItem {
                    VStack {
                        Text("Previous")
                        Image(systemName: "folder")
                    }
                }
        }.accentColor(customTint.color)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
