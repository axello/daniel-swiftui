//
//  Ratings.swift
//  Numbers
//
//  Created by Axel Roest on 08/10/2019.
//  Copyright © 2019 Axel Roest. All rights reserved.
//

import Foundation
import SwiftUI

let ratings = Ratings()

class Ratings: ObservableObject {
    @Published var value = 2.72 {
        didSet {
            print(value.description)
        }
    }
    
    /// only allow the singleton to create itself!
    fileprivate init() {}
}
