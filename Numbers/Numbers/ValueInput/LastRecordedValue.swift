//
//  LastRecordedValue.swift
//  Numbers
//
//  Created by Axel Roest on 08/10/2019.
//  Copyright © 2019 Axel Roest. All rights reserved.
//

import SwiftUI

struct LastRecordedValue: View {
    @ObservedObject private(set) var observedRatings = ratings
    
    var body: some View {
        Text("Last Reading: \(observedRatings.value)")
            .foregroundColor(.secondary)
        .padding()
    }
}

struct LastRecordedValue_Previews: PreviewProvider {
    static var previews: some View {
        LastRecordedValue()
    }
}
