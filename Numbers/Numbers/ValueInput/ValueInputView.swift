//
//  ValueInputView.swift
//  Numbers
//
//  Created by Axel Roest on 08/10/2019.
//  Copyright © 2019 Axel Roest. All rights reserved.
//

import SwiftUI

struct ValueInputView: View {
    @State private var value = 2.0
    @State var showingTintSelector = false
    // bweuh:
    @EnvironmentObject var customTint: CustomTint
    
    var body: some View {
        VStack {
            Spacer()
            ValueView(value: value)
//                .gesture(TapGesture(count: 2).onEnded {
//                    self.showingTintSelector.toggle()
//                })

            ValueSlider(value: $value)
            Spacer()
            HStack {
                Button(action: {
                    self.showingTintSelector.toggle()
                }){
                    Image(systemName: "gear")
                        .foregroundColor(.secondary)
                }
                Spacer()
            }.padding()
        }.sheet(isPresented: $showingTintSelector) {
            TintSelector(showingTintSelector: self.$showingTintSelector)
                .environmentObject(self.customTint)
        }
    }
}

struct ValueInputView_Previews: PreviewProvider {
    static var previews: some View {
        ValueInputView()
    }
}
