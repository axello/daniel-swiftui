//
//  ValueSlider.swift
//  HelloSwiftUI2
//
//  Created by Axel Roest on 07/10/2019.
//  Copyright © 2019 Axel Roest. All rights reserved.
//

import SwiftUI

struct ValueSlider: View {
    @Binding var value: Double
    
    var body: some View {
        Slider(value: $value, in: 0.0 ... 4.0, step: 0.001) { sliderIsEngaged in
            // sliderIsEngaged true if still dragging, false if touchHasEnded
            if !sliderIsEngaged {
                ratings.value = self.value
            }
        }
    }
}

struct ValueSlider_Previews: PreviewProvider {
    static var previews: some View {
        ValueSlider(value: .constant(3.1415))
    }
}
