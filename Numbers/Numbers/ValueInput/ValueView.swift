//
//  ValueView.swift
//  HelloSwiftUI2
//
//  Created by Axel Roest on 07/10/2019.
//  Copyright © 2019 Axel Roest. All rights reserved.
//

import SwiftUI

struct ValueView: View {
    @RoundedTo(1) var value: Double = 3.1415
    
    var body: some View {
        Text(value.description)
    }
}

struct ValueView_Previews: PreviewProvider {
    static var previews: some View {
        ValueView()
    }
}
