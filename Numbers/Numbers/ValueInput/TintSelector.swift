//
//  TintSelector.swift
//  Numbers
//
//  Created by Axel Roest on 08/10/2019.
//  Copyright © 2019 Axel Roest. All rights reserved.
//

import SwiftUI

struct TintSelector: View {
    @EnvironmentObject private var customTint: CustomTint
    @Binding var showingTintSelector: Bool
    
    var body: some View {
        VStack {
            Spacer()
            Text("Selected Tint Color")
                .font(.largeTitle)
                .foregroundColor(customTint.color)
            Picker(selection: $customTint.selectedColor,
                   label: Text("Select a Colour")
                    .multilineTextAlignment(TextAlignment.trailing)) {
                ForEach(0 ..< CustomTint.colors.count) { index in
                    Text(CustomTint.colors[index].description)
                }
            }.pickerStyle(SegmentedPickerStyle())
            Spacer()
            Button(action: {
                self.showingTintSelector = false
            }){
                Text("Dismiss")
                    .accentColor(customTint.color)
            }
        }
    }
}

struct TintSelector_Previews: PreviewProvider {
    static var previews: some View {
        TintSelector(showingTintSelector: .constant(true))
    }
}
