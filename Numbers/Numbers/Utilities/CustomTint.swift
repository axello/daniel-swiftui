//
//  CustomTint.swift
//  Numbers
//
//  Created by Axel Roest on 08/10/2019.
//  Copyright © 2019 Axel Roest. All rights reserved.
//

import SwiftUI

private let defaultColor = Color.blue

/// Create a custom tint which can be selectable from a list by an index value
class CustomTint: ObservableObject {
    static let colors: [Color] = [defaultColor, .green, .orange, .purple, .red]
    
    @Published private(set) var color: Color = defaultColor
    
    var selectedColor: Int {
        get {
            CustomTint.colors.firstIndex(of: color) ?? 0
        }
        set {
            if (0 ..< CustomTint.colors.count)
                .contains(newValue) {
                color = CustomTint.colors[newValue]
            } else {
                color = defaultColor
            }
        }
    }
}
