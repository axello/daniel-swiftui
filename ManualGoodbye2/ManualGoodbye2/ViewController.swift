//
//  ViewController.swift
//  ManualGoodbye1
//
//  Created by Axel Roest on 03/10/2019.
//  Copyright © 2019 Axel Roest. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    private let greetingLabel = GreetingLabel(displaying: "Hello")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Create Stackview
        let stackView = VerticalStackView(in: view, containing: greetingLabel, GreetingButton(title: "Press Here", target: self, selector: #selector(greet)))
        stackView.centered()
    }

    @objc private func greet(_ button: UIButton) {
        button.isEnabled = false
        greetingLabel.text = "Hello, World!"
    }
}

