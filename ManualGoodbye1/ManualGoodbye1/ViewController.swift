//
//  ViewController.swift
//  ManualGoodbye1
//
//  Created by Axel Roest on 03/10/2019.
//  Copyright © 2019 Axel Roest. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    private let greetingLabel = UILabel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Create Stackview
        let stackView = UIStackView()
        view.addSubview(stackView)
        
        stackView.axis = .vertical
        stackView.alignment = .center
        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        stackView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        stackView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        
        // Add Label
        greetingLabel.textColor = .secondaryLabel
        greetingLabel.font = UIFont.preferredFont(forTextStyle: .title1)
        greetingLabel.text = "Hello"
        
        stackView.addArrangedSubview(greetingLabel)
        
        // Add Button
        let greetingButton = UIButton(type: .roundedRect)
        greetingButton.setTitle("Press Here", for: .normal)
        greetingButton.addTarget(self, action: #selector(greet), for: .touchUpInside)
        stackView.addArrangedSubview(greetingButton)
    }

    @objc private func greet(_ button: UIButton) {
        button.isEnabled = false
        greetingLabel.text = "Hello, World!"
    }
}

