//
//  ContentView.swift
//  HelloSwiftUI
//
//  Created by Axel Roest on 03/10/2019.
//  Copyright © 2019 Axel Roest. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        // paddingText()
        CheckerBoard(backgroundColor: .green) {
            Text("SwiftUI Steent")
            Image("Cover")
            //Rectangle().foregroundColor(Color.yellow)
        }
    }
    
    var aspectRatio: CGFloat {
        let size = UIImage(named: "Cover")?.size ?? CGSize(width: 1, height: 1)
        return size.width/size.height
    }
    
    func paddingText() -> some View {
        Text("An Introduction to SwiftUI")
            .padding()
            .background(Color.orange)
            
            .font(.largeTitle)
            .padding()
            .background(Color.blue)
            
            .foregroundColor(.secondary)
        .padding()
        .background(Color.green)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

