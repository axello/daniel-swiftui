//
//  CheckerBoard.swift
//  HelloSwiftUI
//
//  Created by Axel Roest on 04/10/2019.
//  Copyright © 2019 Axel Roest. All rights reserved.
//

import SwiftUI

struct CheckerBoard<A, B>: View where A: View, B: View {
    let firstView: A
    let secondView: B
    let stackColor: Color
    
    var body: some View {
        VStack {
            HStack {
                firstView
                secondView
            }
            HStack {
                secondView
                firstView
            }
        }.background(stackColor)
    }
    
    // Builder
    init(backgroundColor: Color = .white,
         @CheckerBoardBuilder builder: () -> (A, B)) {
        stackColor = backgroundColor
        (firstView, secondView) = builder()
    }

}

// Conditional Conformance
extension CheckerBoard where B == Rectangle {
    init(backgroundColor: Color = .white,
         @CheckerBoardBuilder builder: () -> (A)) {
        stackColor = backgroundColor
        firstView = builder()
        secondView = Rectangle()    // .foregroundColor(Color.black)
    }
}

struct CheckerBoard_Previews: PreviewProvider {
    static var previews: some View {
        CheckerBoard {
            Image("Cover")
            Rectangle().foregroundColor(Color.red)
        }
    }
}

@_functionBuilder
struct CheckerBoardBuilder {
    static func buildBlock<A: View, B: View>(_ firstView: A, _ secondView: B) -> (A, B) {
        return (firstView, secondView)
    }

    // single argument buildblock, fill the other with a black rectangle
    static func buildBlock<A: View>(_ firstView: A) -> A {
        firstView
    }
}
