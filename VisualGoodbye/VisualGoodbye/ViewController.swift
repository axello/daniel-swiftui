//
//  ViewController.swift
//  VisualGoodbye
//
//  Created by Axel Roest on 03/10/2019.
//  Copyright © 2019 Axel Roest. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet private weak var greetingLabel: UILabel!
    
    @IBAction private func greet(_ button: UIButton) {
        button.isEnabled = false
        greetingLabel.text = "Hello, World!"
    }

}

